import firebase from "firebase/app";
import "firebase/firestore";
import config from "./config-dev.js";
if (!firebase.apps.length) {
  console.log(`firebase default`);
  firebase.initializeApp(config);
}

const fstore = firebase.firestore();
fstore.settings({ timestampsInSnapshots: true });
export { firebase, fstore };
