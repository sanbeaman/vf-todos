import React, { memo, useState } from "react";
import TodoListItem from "./TodoListItem";
import { Pane, Button, Card, Text, Pill } from "evergreen-ui";
import { SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE } from "../constants/Filters";

const TodoList = memo(
  ({ todos, deleteItem, toggleItem, toggleAllTodo, clearCompleted }) => {
    const [visibilityFilter, setFilter] = useState(SHOW_ALL);

    const todosCount = todos.length;
    // const completedIds = todos
    //   .filter(item => item.isComplete)
    // console.log(`completedIds=${completedIds}`);
    // const completedCount = completedIds.length;
    const completedCount = todos.filter(item => item.isComplete).length;
    // const completedCount = todos.filter(({ completed }) => completed).length;
    const activeCount = todosCount - completedCount;
    let visibleTodos;
    switch (visibilityFilter) {
      case SHOW_ALL:
        visibleTodos = todos;
        break;
      case SHOW_COMPLETED:
        visibleTodos = todos.filter(item => item.isComplete);
        break;
      case SHOW_ACTIVE:
        visibleTodos = todos.filter(item => !item.isComplete);
        break;
      default:
        throw new Error("Unknown filter: " + visibilityFilter);
    }

    // const onItemRemove = itemId => {
    //   ref.doc(itemId).delete();
    // };
    //
    // const onItemCheck = (itemId, complete) => {
    //   if (complete) {
    //     ref.doc(itemId).update({
    //       isComplete: false
    //     });
    //   } else {
    //     ref.doc(itemId).update({
    //       isComplete: true
    //     });
    //   }
    // };

    return (
      <>
        {todosCount > 0 && (
          <Pane
            paddingX={24}
            paddingY={0}
            margin={0}
            display="flex"
            justifyContent="flex-end"
            alignItems="center"
            flexDirection="row"
          >
            <Text>Total:</Text>

            <Pill display="inline-flex" margin={8} color="neutral" isSolid>
              {todosCount}
            </Pill>
            <Text>Completed:</Text>
            <Pill display="inline-flex" margin={8} color="blue" isSolid>
              {completedCount}
            </Pill>
            <Text>Active:</Text>
            <Pill display="inline-flex" margin={8} color="green" isSolid>
              {activeCount}
            </Pill>
          </Pane>
        )}
        <Card
          elevation={2}
          display="flex"
          flexDirection="column"
          paddingY={12}
          marginTop={12}
          background="#A6B1BB"
        >
          {todosCount > 0 && (
            <Pane
              paddingX={12}
              paddingY={0}
              margin={0}
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              flexDirection="row"
            >
              <Pane>
                {visibilityFilter !== SHOW_ALL ? (
                  <Button onClick={() => setFilter(SHOW_ALL)}>Show All</Button>
                ) : (
                  <Button disabled>Show All</Button>
                )}
              </Pane>
              <Pane>
                {visibilityFilter !== SHOW_COMPLETED ? (
                  <Button
                    appearance="primary"
                    onClick={() => setFilter(SHOW_COMPLETED)}
                  >
                    Show Completed
                  </Button>
                ) : (
                  <Button iappearance="primary" disabled>
                    Show Completed
                  </Button>
                )}
                {visibilityFilter !== SHOW_ACTIVE ? (
                  <Button
                    appearance="primary"
                    intent="success"
                    onClick={() => setFilter(SHOW_ACTIVE)}
                  >
                    Show Active
                  </Button>
                ) : (
                  <Button appearance="primary" intent="success" disabled>
                    Show Active
                  </Button>
                )}
              </Pane>
              <Pane>
                {completedCount > 0 ? (
                  <Button
                    appearance="primary"
                    intent="danger"
                    onClick={() => clearCompleted()}
                  >
                    Clear Completed
                  </Button>
                ) : (
                  <Button appearance="primary" intent="danger" disabled>
                    Clear Completed
                  </Button>
                )}
              </Pane>
            </Pane>
          )}

          {visibleTodos.length > 0 && (
            <Pane paddingX={12} paddingY={0}>
              {visibleTodos.map(item => (
                <TodoListItem
                  {...item}
                  key={item.uid}
                  divider={item.uid !== visibleTodos.length - 1}
                  onButtonClick={() => deleteItem(item.uid)}
                  onCheckBoxToggle={() => toggleItem(item.uid, item.isComplete)}
                />
              ))}
            </Pane>
          )}
        </Card>
      </>
    );
  }
);

export default TodoList;
