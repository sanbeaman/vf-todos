import React, { memo } from "react";
// import { TextField, Paper, Button, Grid } from "@material-ui/core";
import { Pane, Button, Card, TextInput } from "evergreen-ui";
const AddTodo = memo(props => (
  <Card
    background="#A6B1BB"
    elevation={2}
    display="flex"
    paddingX={12}
    paddingY={12}
    marginBottom={12}
  >
    <Pane display="flex" flex={1}>
      <TextInput
        flex={1}
        name="text-input-name"
        placeholder="Text input placeholder..."
        value={props.inputValue}
        onChange={props.onInputChange}
        onKeyPress={props.onInputKeyPress}
        marginRight={12}
      />
    </Pane>

    <Button appearance="primary" intent="success" onClick={props.onButtonClick}>
      Add
    </Button>
  </Card>
));

export default AddTodo;
