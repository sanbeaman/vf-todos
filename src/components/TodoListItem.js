import React, { memo } from "react";
import { Pane, Card, Switch, Text, IconButton } from "evergreen-ui";
// import {
//   List,
//   ListItem,
//   Checkbox,
//   IconButton,
//   ListItemText,
//   ListItemSecondaryAction
// } from "@material-ui/core";
// import DeleteOutlined from "@material-ui/icons/DeleteOutlined";

const TodoListItem = memo(props => (
  <Card
    border
    display="flex"
    padding={16}
    marginY={12}
    elevation={1}
    borderRadius={8}
    background="purpleTint"
    alignItems="center"
  >
    <Switch
      height={24}
      marginRight={24}
      onClick={props.onCheckBoxToggle}
      checked={props.isComplete}
    />
    <Pane flex={1} display="flex">
      <Text>{props.item} </Text>
    </Pane>
    <IconButton icon="trash" intent="danger" onClick={props.onButtonClick} />
    {/* <Button height={24} onClick={props.onButtonClick}>
      <Icon icon="trash" />
    </Button>  */}
  </Card>
));

export default TodoListItem;
