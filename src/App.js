import React, { memo, useEffect, useState } from "react";
import {
  Pane,
  Card,
  Heading,
  CornerDialog,
  Icon,
  Button,
  Text
} from "evergreen-ui";
import { fstore } from "./api/firebase";
import TodoList from "./components/TodoList";
import AddTodo from "./components/AddTodo";
import useInputFieldValue from "./hooks/useInputFieldValue";

const App = memo(() => {
  const {
    inputValue,
    changeInput,
    clearInput,
    keyInput
  } = useInputFieldValue();
  const [allitems, setAllItems] = useState([]);

  const ref = fstore.collection(`todos`);

  useEffect(
    () => {
      ref.onSnapshot(function(snapshot) {
        setAllItems(
          snapshot.docs.map(function(documentSnapshot) {
            return { uid: documentSnapshot.id, ...documentSnapshot.data() };
          })
        );
      });
    },
    [fstore]
  );
  const [showDialog, setDialogVal] = useState(false);
  const toggleDialog = currentState => {
    setDialogVal(!currentState);
  };

  const clearInputAndAddTodo = _ => {
    clearInput();
    const todoItem = {
      item: inputValue,
      isComplete: false,
      createdAt: new Date()
    };
    fstore.collection("todos").add(todoItem);
  };

  const onItemRemove = itemId => {
    ref.doc(itemId).delete();
  };

  const onItemCheck = (itemId, complete) => {
    if (complete) {
      ref.doc(itemId).update({
        isComplete: false
      });
    } else {
      ref.doc(itemId).update({
        isComplete: true
      });
    }
  };

  const onClearCompleted = () => {
    ref
      .where("isComplete", "==", true)
      .get()
      .then(function(qs) {
        var batch = fstore.batch();
        qs.forEach(function(doc) {
          batch.delete(doc.ref);
        });

        //commit the batch deleteItem
        return batch.commit();
      });
  };

  return (
    <Pane
      margin={12}
      padding={12}
      display="flex"
      justifyContent="space-between"
      alignItems="stretch"
      flexDirection="column"
      border="default"
      background="tint1"
    >
      <Card display="flex" justifyContent="center" paddingY={12}>
        <Pane
          flex={4}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <Heading size={700}>VF Todos</Heading>
        </Pane>
        <Pane alignSelf="flex-end">
          <Button
            appearance="minimal"
            height={32}
            onClick={() => toggleDialog(showDialog)}
          >
            <Icon icon="info-sign" color="info" size={24} />
          </Button>
        </Pane>
      </Card>
      <Card
        display="flex"
        flexDirection="column"
        justifyContent="center"
        paddingY={12}
      >
        <AddTodo
          inputValue={inputValue}
          onInputChange={changeInput}
          onButtonClick={clearInputAndAddTodo}
          onInputKeyPress={event => keyInput(event, clearInputAndAddTodo)}
        />
        <TodoList
          todos={allitems}
          deleteItem={onItemRemove}
          toggleItem={onItemCheck}
          clearCompleted={onClearCompleted}
        />
      </Card>
      <Card display="flex" justifyContent="center" paddingY={12}>
        <Text color="muted">
          &#169; 2018 Drew Beaman &#8226; drew@sanbeaman.com
        </Text>
      </Card>
      <CornerDialog
        title="Drew Beaman Code Challenge"
        isShown={showDialog}
        onCloseComplete={() => toggleDialog(true)}
        hasFooter={false}
      >
        I didn't want to just do a basic todo list. So I used my limited time to
        build a todo list using some new/experimental technology that I wanted
        to test. So this Todo List, uses the new ReactJS hooks useState and
        useEffects, Evergreen UI Framework, and Firebase's Firestore NoSQL
        database.
      </CornerDialog>
    </Pane>
  );
});

export default App;
